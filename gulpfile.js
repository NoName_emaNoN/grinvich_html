var gulp = require('gulp'); // Сообственно Gulp JS
var open = require('gulp-open');
var fs = require('fs');
//    imagemin = require('gulp-imagemin'), // Минификация изображений
var uglify = require('gulp-uglify'); // Минификация JS
var concat = require('gulp-concat'); // Склейка файлов
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var del = require('del');

var cleanCSS = require('gulp-clean-css'); // сжимает, оптимизирует
//var minifyCSS = require('gulp-minify-css');  // сжимает, оптимизирует
var less = require('gulp-less');
var path = require('path');
var rename = require("gulp-rename");  // переименовывает

var fileExists = require('file-exists');
var autoprefixer = require('gulp-autoprefixer');

var paths = {
    scripts: ['src/js/*.js', 'src/less/blocks/*', 'src/less/blocks/**/*.js'],
    images: ['./src/img/*.{jpg,jpeg,png,gif}'],
    less: ['src/less/bootstrap/variables.less', 'src/less/*.less', 'src/less/blocks/*', './src/less/blocks/**/*.less']
};
/**
 *
 * COMMON
 *
 */

function log(error) {
    console.log([
        '',
        "----------ERROR MESSAGE START----------",
        ("[" + error.name + " in " + error.plugin + "]"),
        error.message,
        "----------ERROR MESSAGE END----------",
        ''
    ].join('\n'));

    this.emit('end');
}

/**
 *
 * TASKS
 *
 */

gulp.task('clean:css:bootstrap', function (cb) {
    del(['build/css/bootstrap*.css'], cb);
});

gulp.task('clean:css:my', function (cb) {
    del(['build/css/*.css', '!build/css/bootstrap*.css'], cb);
});

gulp.task('clean:css', ['clean:css:bootstrap', 'clean:css:my']);

gulp.task('clean:js', function (cb) {
    del(['build/js/*.js'], cb);
});

gulp.task('clean:img', function (cb) {
    del(['build/img/*.{jpg,jpeg,png,gif}'], cb);
});

gulp.task('clean', ['clean:css', 'clean:js', 'clean:img']);

gulp.task('bootstrap', ['clean:css:bootstrap'], function () {
    return gulp.src(['./src/less/bootstrap/bootstrap.less'])
        //.pipe(concat('bootstrap.less'))
        .pipe(less({
            paths: [path.join('bower_components/bootstrap/less')]
        }))
        .on('error', log)
        .pipe(concat('bootstrap.css'))
        .pipe(gulp.dest('./build/css'))
        .pipe(cleanCSS())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest('./build/css'))
        .pipe(reload({stream: true}));
});

gulp.task('less', ['clean:css:my'], function () {
    return gulp.src(paths.less)
        .pipe(concat('common.less'))
        .pipe(less({
            paths: [path.join('./src/less/bootstrap'), 'bower_components/bootstrap/less']
        }))
        .on('error', log)
        .pipe(concat('common.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest('./build/css'))
        .pipe(reload({stream: true}));
});


gulp.task('js', ['clean:js'], function () {
    gulp.src(paths.scripts)
        .pipe(concat('script.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(uglify())
        .on('error', log)
        .pipe(gulp.dest('./build/js'))
        .pipe(reload({stream: true}));
});

gulp.task('img', ['clean:img'], function () {
    gulp.src(paths.images)
        .pipe(gulp.dest('./build/img'));
});

// watch files for changes and reload
gulp.task('watch', ['build'], function () {
    browserSync.init({
        server: {
            baseDir: '.'
        }
    });

    gulp.src('./*.html')
        .pipe(open('http://localhost:3000/<%=file.path.replace(file.base,"")%>', {app: 'chrome'}));

    gulp.watch(['*.html']).on('change', reload);
    gulp.watch(['src/less/bootstrap/**/*.less'], {cwd: '.'}, ['bootstrap']);
    gulp.watch(paths.less, {cwd: '.'}, ['less']).on('error', log);
    gulp.watch(paths.scripts, {cwd: '.'}, ['js']);
    gulp.watch(paths.images, {cwd: '.'}, ['img', reload]);
});

gulp.task('build', ['clean', 'bootstrap', 'less', 'js', 'img'], function () {

});

gulp.task('default', ['build'], function () {
    // place code for your default task here
});


var minimist = require('minimist');

gulp.task('block', function () {
    var argv = require('minimist')(process.argv.slice(2));
    var mkdirp = require('mkdirp');
    var open = require("open");

    var blockName = (typeof(argv.b) == 'undefined') ? argv._[1] : argv.b;

    if (typeof blockName == 'undefined') {
        return console.log('get me a block name!');
    }

    blockName = 'b-' + blockName;
    var blockPath = 'src/less/blocks/' + blockName + '/';
    var lessFilename = blockPath + blockName + '.less';

    fs.exists(blockPath, function (exists) {
        if (exists) {
            // Do something
            console.log('This block already exists!');
        } else {
            mkdirp(blockPath, function (err) {
                if (err) console.error(err)
                else {
                    fs.writeFile(lessFilename, '.' + blockName + ' {\n\n}');

                    open(lessFilename, "PhpStorm.exe");
                }
            });

        }
    });
});