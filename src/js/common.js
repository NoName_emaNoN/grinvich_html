$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
$(function () {
    $('select:not(.selectpicker-ignore)').selectpicker();

    if ($.fn.datepicker) {
        $('.js-datepicker').datepicker({
            language: 'ru',
            format: 'dd.mm.yyyy',
            autoclose: true
        });
    }
});
