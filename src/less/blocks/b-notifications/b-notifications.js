$(function () {
    $('.b-notifications').each(function () {
        var msnry = new Masonry(this, {
            itemSelector: '.b-notifications__item',
            columnWidth: 240,
            isResizable: false
        });
    });
});