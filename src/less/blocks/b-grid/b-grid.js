$(document).on('click', '.b-grid__row', function (e) {
    var href = $(this).data('href');

    if (href && href.length && $(e.target).closest('.b-grid__status-column').length == 0) {
        console.log('We have a href!', e.target);
        window.location.href = href;
    }
});
