$(function () {

    var mouseInLeftMenu = false;
    var isMenuOpen = false;
    var timer;

    $('.b-left-menu__trigger').on('mouseenter', function () {
        $(this).closest('.b-left-menu').toggleClass('b-left-menu_extended');
        isMenuOpen = true;
    });

    $('.b-left-menu').on('mouseleave', function () {
        mouseInLeftMenu = false;

        var menu = $(this);

        clearTimeout(timer);
        timer = setTimeout(function () {
            if (!mouseInLeftMenu && isMenuOpen) {
                menu.toggleClass('b-left-menu_extended');
            }
        }, 500);
    }).on('mouseenter', function () {
        mouseInLeftMenu = true;
        clearTimeout(timer);
    });
});