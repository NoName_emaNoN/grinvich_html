$(function () {
    $('.js-add-employer').on('click', function (e) {
        e.preventDefault();

        $('.js-employer-list-original').clone().removeClass('js-employer-list-original hidden').insertBefore($(this).closest('.row'));
    });

    $(document).on('click', '.js-remove-employer', function (e) {
        e.preventDefault();

        $(this).closest('.form-group').remove();
    });
});